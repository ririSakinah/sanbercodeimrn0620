//nomor 1
function teriak() {
  return "hallo guys";
}
console.log(teriak());

//nomor 2
function kalikan(a, b) {
  return a * b;
}
var num1 = 4;
var num2 = 12;
console.log(kalikan(num1, num2));

//nomor 3
function introduce(nama, umur, alamat, hobi) {
  this.name = nama;
  this.age = umur;
  this.address = alamat;
  this.hobby = hobi;
}

var perkenalan = new introduce(
  "Agus",
  30,
  "Jln. Malioboro, Yogyakarta",
  "gamming"
);
console.log(
  "Nama saya " +
    perkenalan.name +
    ", umur saya " +
    perkenalan.age +
    " tahun, alamat saya di " +
    perkenalan.address +
    ", dan saya punya hobby yaitu " +
    perkenalan.hobby +
    "!"
);
