//NOMOR 1
class Animal {
    constructor(nama) {
      this.nama = nama;
    }
    introduce() {
      return "Hello, my name is ${this.name}";
    }
  }
  
  class Sheep extends Animal{
    constructor(nama,kaki,cold_blooded){
      super(nama)
      this.kaki = 4;
      this.cold_blooded = false
    }
  }

  var sheep = new Sheep("shaun");
  sheep.introduce()
  console.log(sheep.nama) // "shaun"
  console.log(sheep.kaki) // 4
  console.log(sheep.cold_blooded) // false

  class Ape extends Animal{
    constructor(nama){
      super(nama)
    } yell(){return console.log("Auooo")}
    }

  class Frog extends Animal{
    constructor(nama,jump){
      super(nama)
    } jump(){return console.log("hop-hop")}
  }

  var sungokong = new Ape("kera sakti")
  sungokong.yell() // "Auooo"
   
  var kodok = new Frog("buduk")
  kodok.jump() // "hop hop"


//NOMOR 2
  class Clock {
    constructor({ template }) {
      this.template = template;
    } 
    render() {
      var date = new Date(); 
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours; 
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
      var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
      console.log(output);
    }
  
    stop() {
      clearInterval(this.timer);
    }
  
    start() {
      this.render();
      this.timer = setInterval(() => this.render(), 1000);
    }
  }

  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 
  
  