var now = new Date();
var thisYear = now.getFullYear(); // 2020 (tahun sekarang)
var obj = {};
var arrobj = [];
//NOMOR 1
function arrayToObject(arr) {
  for (var i = 0; i < arr.length; ++i) {
    var age;
    thn = arr[i][3];
    if (thn >= 0 && thn <= 2020) {
      age = 2020 - thn;
    } else {
      age = "invalid birth year";
    }

    var obj = {
      firstname: arr[i][0],
      lastname: arr[i][1],
      gender: arr[i][2],
      age: age,
    };
    arrobj[obj.firstname + " " + obj.lastname] = obj;
  }

  console.log(arrobj);
}

// Driver Code
var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
arrayToObject(people);

var people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023],
];
arrayToObject(people2);
arrayToObject([]);

//NOMOR 2
function shoppingTime(memberId, money) {
  var param = (memberId, money);
  var memberid = memberId;

  var beli = [];
  var total = 0;
  var arrayHarga = [1500000, 500000, 250000, 175000, 50000];
  arrobj = [
    "Sepatu Stacattu",
    "Baju Zoro",
    "Baju H&N",
    "Sweater Uniklooh",
    "Casing Handphone",
  ];

  if (param == undefined || memberid == "") {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  } else if (money < 50000) {
    return "Mohon maaf, uang tidak cukup";
  } else {
    for (var i = 0; i < 5; i++) {
      if (money >= arrayHarga[i]) {
        total = total + arrayHarga[i];
        changeMoney = money - total;
        beli.push(arrobj[i]);
      }
    }
    var belanja = {};
    belanja.memberId = memberid;
    belanja.money = money;
    belanja.listPurchased = beli;
    belanja.changeMoney = changeMoney;
  }
  return belanja;
}

// TEST CASES
console.log(shoppingTime("1820RzKrnWn08", 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime("82Ku8Ma742", 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime("", 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime("234JdhweRxa53", 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//NOMOR 3
function naikAngkot(arrPenumpang) {
  rute = ["A", "B", "C", "D", "E", "F"];
  var ob = {};
  var arrayobject = [];

  for (var i = 0; i < arrPenumpang.length; i++) {
    ob = {};
    for (var j = 0; j < rute.length; j++) {
      if (arrPenumpang[i][2] === rute[j]) {
        var a = j;
      } else if (arrPenumpang[i][1] === rute[j]) {
        var b = j;
      }
    }

    var uang = (a - b) * 2000;

    ob.penumpang = arrPenumpang[i][0];
    ob.naikDari = arrPenumpang[i][1];
    ob.tujuan = arrPenumpang[i][2];
    ob.bayar = uang;

    arrayobject.push(ob);
  }
  return arrayobject;
}

//TEST CASE
console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"],
  ])
);
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]
