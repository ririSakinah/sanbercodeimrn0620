function DescendingTen(num) {
  a = [];
  if (num != undefined) {
    for (i = num; i > num - 10; i--) {
      a.push(i);
    }
  } else {
    a.push(-1);
  }
  return a.map(String);
}

function AscendingTen(num) {
  a = [];
  if (num != undefined) {
    for (i = num; i < num + 10; i++) {
      a.push(i);
    }
  } else {
    a.push(-1);
  }
  return a.map(String);
}

function ConditionalAscDesc(reference, check) {
  a = [];

  if (check == undefined) {
    a.push(-1);
  } else if (check % 2 == 0) {
    for (i = reference; i > reference - 10; i--) {
      a.push(i);
    }
  } else {
    for (i = reference; i < reference + 10; i++) {
      a.push(i);
    }
  }
  return a;
}

// TEST CASES Descending Ten
console.log(DescendingTen(100)); // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)); // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()); // -1

// TEST CASES Ascending Ten
console.log(AscendingTen(11)); // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)); // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()); // -1

// TEST CASES Conditional Ascending Descending
console.log(ConditionalAscDesc(20, 8)); // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)); // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)); // -1
console.log(ConditionalAscDesc()); // -1
