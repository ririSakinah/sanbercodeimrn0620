//JAWABAN NO 1
function range(a, b) {
  var c = [];
  if (b == undefined) {
    c.push(-1);
  } else if (a < b) {
    for (i = a; i <= b; i++) {
      c.push(i);
    }
  } else if (a > b) {
    for (i = a; i >= b; i--) {
      c.push(i);
    }
  }
  return c;
}
console.log("NOMOR 1");
console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1

//JAWABAN NO 2
function rangewithstep(startNum, finishNum, step) {
  var c = [];
  if (startNum < finishNum) {
    for (i = startNum; i <= finishNum; i += step) {
      c.push(i);
    }
  } else if (startNum > finishNum) {
    for (i = startNum; i >= finishNum; i -= step) {
      c.push(i);
    }
  }
  return c;
}

console.log("NOMOR 2");
console.log(rangewithstep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangewithstep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangewithstep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangewithstep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]

//JAWABAN NO 3
function sum(startNum, finishNum, step) {
  var c = 0;
  if (step == undefined) {
    step = 1;
    if (startNum < finishNum) {
      for (i = startNum; i <= finishNum; i += step) {
        c = c + i;
      }
    } else if (startNum > finishNum) {
      for (i = startNum; i >= finishNum; i -= step) {
        c = c + i;
      }
    } else if (startNum != undefined || finishNum != undefined) {
      c = 1;
    } else {
      c = 0;
    }
  } else if (step != undefined) {
    if (startNum < finishNum) {
      for (i = startNum; i <= finishNum; i += step) {
        c = c + i;
      }
    } else if (startNum > finishNum) {
      for (i = startNum; i >= finishNum; i -= step) {
        c = c + i;
      }
    }
  }
  return c;
}
console.log("NOMOR 3");
console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)); // 1
console.log(sum()); // 0

//JAWABAN NO 4
function dataHandling() {
  var a = "";
  var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
  ];
  for (i = 0; i < input.length; i++) {
    for (j = 0; j < input[i].length; j++) {
      var id = input[i][0];
      var nama = input[i][1];
      var alamat = input[i][2];
      var tgl = input[i][3];
      var hobi = input[i][4];
    }
    console.log("no id : " + id);
    console.log("nama : " + nama);
    console.log("alamat : " + alamat);
    console.log("tgl. lahir : " + tgl);
    console.log("hobi : " + hobi);
    console.log(" ");
  }
  return a;
}
console.log("NOMOR 4");
console.log(dataHandling());

//JAWABAN NO 5
function balikkata(kata) {
  var a = "";
  var b = kata;
  for (i = kata.length - 1; i >= 0; i--) {
    a = a + b[i];
  }
  return a;
}
console.log("NOMOR 5");
console.log(balikkata("Kasur Rusak")); // kasuR rusaK
console.log(balikkata("SanberCode")); // edoCrebnaS
console.log(balikkata("Haji Ijah")); // hajI ijaH
console.log(balikkata("racecar")); // racecar
console.log(balikkata("I am Sanbers")); // srebnaS ma I

//JAWABAN NO 6
function dataHandling2() {
  var space = "";
  var input = [
    "0001",
    "Roman Alamsyah ",
    "Bandar Lampung",
    "21/05/1989",
    "Membaca",
  ];

  input.splice(
    1,
    4,
    "Roman Alamsyah Elsharawy",
    "Provinsi Bandar Lampung",
    "21/05/1989",
    "Pria",
    "SMA Internasional Metro"
  );

  var slice = input[1].slice(0, 15);

  var a = input[3].split("/").map(Number);
  var join = a.join("-");
  var bulan = a[1];
  switch (bulan) {
    case 1: {
      bulan = "Januari";
      break;
    }
    case 2: {
      bulan = "Februari";
      break;
    }
    case 3: {
      bulan = "Maret";
      break;
    }
    case 4: {
      bulan = "april";
      break;
    }
    case 5: {
      bulan = "mei";
      break;
    }
    case 6: {
      bulan = "juni";
      break;
    }
    case 7: {
      bulan = "juli";
      break;
    }
    case 8: {
      bulan = "agustus";
      break;
    }
    case 9: {
      bulan = "september";
      break;
    }
    case 10: {
      bulan = "oktober";
      break;
    }
    case 11: {
      bulan = "nopember";
      break;
    }
    case 12: {
      bulan = "desember";
      break;
    }
  }

  console.log(input);
  console.log(bulan);
  console.log(a.sort());
  console.log(join);
  console.log(slice);
  return space;
}
console.log("NOMOR 5");
console.log(dataHandling2());
