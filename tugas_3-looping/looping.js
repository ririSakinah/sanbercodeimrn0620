//Jawaban no 1
console.log("JAWABAN NO 1");
var i = 2;
console.log("LOOPING PERTAMA");
while (i <= 20) {
  console.log(i + " - I love coding");
  i = i + 2;
}

var j = 20;
console.log("LOOPING KEDUA");
while (j >= 2) {
  console.log(j + " - I will become a mobile developer");
  j = j - 2;
}

//Jawaban no 2
console.log("JAWABAN NO 2");
for (var a = 1; a <= 20; a++) {
  if (a % 2 == 1) {
    if (a % 3 == 0) {
      console.log(a + " - I Love Coding");
    } else {
      console.log(a + " - santai");
    }
  } else if (a % 2 == 0) {
    console.log(a + " - berkualitas");
  }
}

//JAWABAN NO 3
console.log("JAWABAN NO 3");
for (var b = 1; b <= 4; b++) {
  var nilai = "";
  for (var c = 1; c <= 8; c++) {
    nilai += "#";
  }
  console.log(nilai);
}

//JAWABAN NO 4
console.log("JAWABAN NO 4");
for (var k = 1; k <= 7; k++) {
  var nilai = "";
  for (var l = 1; l <= k; l++) {
    nilai += "#";
  }
  console.log(nilai);
}

//JAWABAN NOMOR 5
console.log("JAWABAN NO 5");
for (var b = 1; b <= 8; b++) {
  var nilai = "";
  for (var c = 1; c <= 4; c++) {
    if (b % 2 === 0) {
      nilai += "# ";
    } else {
      nilai += " #";
    }
  }
  console.log(nilai);
}
