// di index.js
var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
  //{name: 'BUKU', timeSpent: 10500}
];

// Tulis code untuk memanggil function readBooks di sini

// books.forEach((a, b) =>
//   readBooks(b === 0 ? 10000 : time - a.timeSpent, a, (cb) => {
//     console.log(cb);
//   })
// );

//console.log(books[1].timeSpent);
// time =10000
// books.forEach((isi) =>
//   readBooks(time - books[1].timeSpent, isi, (cb) => {
//     console.log(cb);
//     console.log(time);
//   })
// );

time = 10000;
function hitung(urut) {
  if (urut == books.length) {
    return 0;
  } else {
    //readBooks(time, books[urut], (cb) => console.log(cb));
    readBooks(time, books[urut], function (cek) {
      time = time - books[urut].timeSpent;
      hitung(urut + 1);
    });
  }
}
hitung(0);

// function Callback(callback){
//     setTimeout(function(){
//         callback(books);
//     },10000)
// }

//var result=readBooks
//console.log(result)
