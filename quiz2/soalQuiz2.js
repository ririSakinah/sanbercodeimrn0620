class Score {
  constructor(subject, point, email) {
    this.subject = subject;
    this.point = point;
    this.email = email;

    class Average extends Score(point) {
      constructor(subject, point, email) {
        return Math.round(
          point.reduce((first, next) => first + next, 0) / point.length
        );
      }
    }
  }
}

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93],
];

function viewScores(data, subject) {
  // code kamu di sini
}

// TEST CASE
viewScores(data, "quiz-1");
viewScores(data, "quiz-2");
viewScores(data, "quiz-3");

function recapScores(data) {
  // code kamu di sini
}

recapScores(data);
