function ask(question, good, bad) {
    if (confirm(question)) {
        good();
    }
    else { 
        bad();
    }
  }
  
  function yes() {
    console.log( "Glad to hear that!" );
  }
  
  function no() {
    console.log( "Sorry to hear that" );
  }
  
  ask("Are you ok?", yes, no);
  