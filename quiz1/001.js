function terbalik(kata) {
  var a = "";
  var b = kata;
  for (i = kata.length - 1; i >= 0; i--) {
    a = a + b[i];
  }
  return a;
}

function maksimum(b, c) {
  var a = [];

  if (b > c) {
    a.push(b);
    console.log(a);
  } else if (b == undefined) {
    a.push(-1);
  } else if (b < c) {
    a.push(c);
  } else if (b == c) {
    a.push(b);
  } else if (c == undefined) {
    a.push(1);
  }
  return a;
}

function palindrome(kata) {
  var abjad = /[^A-Za-z0-9]/g;
  kata = kata.toLowerCase().replace(abjad, "");
  var a = kata.length;
  for (var i = 0; i < a / 2; i++) {
    if (kata[i] !== kata[a - 1 - i]) {
      return false;
    }
  }
  return true;
}

// TEST CASES String Terbalik
console.log(terbalik("abcde")); // edcba
console.log(terbalik("rusak")); // kasur
console.log(terbalik("racecar")); // racecar
console.log(terbalik("haji")); // ijah

// TEST CASES Bandingkan Angka
console.log(maksimum(10, 15)); // 15
console.log(maksimum(12, 12)); // -1
console.log(maksimum(-1, 10)); // -1
console.log(maksimum(112, 121)); // 121
console.log(maksimum(1)); // 1
console.log(maksimum()); // -1
console.log(maksimum("15", "18")); // 18

// TEST CASES Palindrome
console.log(palindrome("kasur rusak")); // true
console.log(palindrome("haji ijah")); // true
console.log(palindrome("nabasan")); // false
console.log(palindrome("nababan")); // true
console.log(palindrome("jakarta")); // false
