var nama = "";
var peran = "";
var hari = 1;
var bulan = 2;
var tahun = 2020;

//jawaban nomor 1
if (nama == "" && peran == "") {
  console.log("Nama harus diisi!");
} else if (nama != "" && peran == "Guard") {
  console.log("Selamat datang di Dunia Werewolf, " + nama);
  console.log(
    "Halo " +
      peran +
      " " +
      nama +
      ",  kamu akan membantu melindungi temanmu dari serangan werewolf."
  );
} else if (nama != "" && peran == "Werewolf") {
  console.log("Selamat datang di Dunia Werewolf, " + nama);
  console.log(
    "Halo " + peran + " " + nama + ",  Kamu akan memakan mangsa setiap malam!"
  );
} else if (nama != "" && peran == "") {
  console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
} else {
  console.log("Selamat datang di Dunia Werewolf, " + nama);
}

//jawaban no 2
switch (bulan) {
  case 1: {
    bulan = "Januari";
    break;
  }
  case 2: {
    bulan = "Februari";
    break;
  }
  case 3: {
    bulan = "Maret";
    break;
  }
  case 4: {
    bulan = "april";
    break;
  }
  case 5: {
    bulan = "mei";
    break;
  }
  case 6: {
    bulan = "juni";
    break;
  }
  case 7: {
    bulan = "juli";
    break;
  }
  case 8: {
    bulan = "agustus";
    break;
  }
  case 9: {
    bulan = "september";
    break;
  }
  case 10: {
    bulan = "oktober";
    break;
  }
  case 11: {
    bulan = "nopember";
    break;
  }
  case 12: {
    bulan = "desember";
    break;
  }
}

console.log("Tanggal : " + hari + " " + bulan + " " + tahun);
